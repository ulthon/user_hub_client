<?php

namespace UserHub;

use GuzzleHttp\Client as GuzzleHttpClient;

class Client
{
    protected $config = [
      'key' => '',
      'secret' => '',
      'host' => 'https://user-hub.ulthon.com'
    ];

    protected $client = null;

    public function __construct($config = [])
    {

        $this->config($config);

        if (empty($this->config['host'])) {
            throw new \Exception("请设置host", 1);
        }

        $this->client = new GuzzleHttpClient([
          "base_uri" => $this->config['host'] . '/api/'
        ]);
    }

    public function config($config)
    {
        $this->config = array_merge($this->config, $config);
        return $this;
    }


    public function key($key = null)
    {
        if (is_null($key)) {
            return $this->config['key'];
        }

        $this->config['key'] = $key;

        return $this;
    }
    public function secret($secret = null)
    {
        if (is_null($secret)) {
            return $this->config['secret'];
        }

        $this->config['secret'] = $secret;

        return $this;
    }
    public function host($host = null)
    {
        if (is_null($host)) {
            return $this->config['host'];
        }

        $this->config['host'] = $host;

        return $this;
    }


    /**
     * 生成提交的form参数
     *
     * @param array $data
     * @return array
     */
    protected function buildFormData($data = [])
    {

        if (isset($data['sign'])) {
            throw new \Exception("不要试图传入sign参数", 1);
        }
        if (isset($data['key'])) {
            throw new \Exception("不要试图传入key参数", 1);
        }

        $require_data = [
          'key' => $this->key(),
          'timestamp' => time()
        ];


        $data = array_merge($require_data, $data);

        // 对数组的值按key排序
        ksort($data);
        // 生成url的形式
        $params = http_build_query($data);
        // 生成sign
        $sign = md5($params . $this->secret());

        $data['sign'] = $sign;

        return $data;
    }


    public function getBowserRedirectUrl($url)
    {
        return $this->host() . '/admin/Auth/show?' . http_build_query([
          'key' => $this->key(),
          'url' => urlencode($url)
        ]);
    }

    public function getUserinfoByCode($code)
    {

        $post_data = [];

        $post_data['code'] = $code;

        return $this->post($post_data, 'User/readByCode');
    }

    public function getUserinfoByUid($uid)
    {
        $post_data = [];

        $post_data['uid'] = $uid;

        return $this->post($post_data, 'User/readByUid');
    }

    public function post($data, $uri)
    {

        $post_data = $this->buildFormData($data);

        $response = $this->client->post($uri, [
          'form_params' => $post_data
        ]);

        $body = (string)$response->getBody();

        $result = json_decode($body, true);

        // TODO:对返回的code进行判断和异常抛出


        return $result['data'];
    }
}
