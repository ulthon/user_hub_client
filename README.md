# user_hub_client

#### 介绍
用户中心的客户端库,封装了易用的接口.

#### 软说明

安装方式:

```
composer require ulthon/user_hub_client
```

TP6中简单使用:

```
    // 判断是否需要登录
    if (empty(Session::get('user_uid'))) {
      $code = $this->request->param('code');

      // 实例化客户端,传入相关参数
      $user_hub_client = new Client([
        'key' => get_system_config('user_hub_key'),
        'secret' => get_system_config('user_hub_secret'),
        'host' => get_system_config('user_hub_host'),     // http://user-hub.ulthon.com ,填入自己搭建好的UserHub站点地址,需要带协议
      ]);
      if (empty($code)) {
        // 跳转登录
        $url = $user_hub_client->getBowserRedirectUrl($this->request->url(true));
        return $this->error('请登录', $url);
      } else {
        // 获取用户信息
        $user_info = $user_hub_client->getUserinfoByCode($code);
        Session::set('user_uid', $user_info['uid']);
      }
      

    }
  }
```